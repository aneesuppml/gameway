class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.integer :quantity
      t.decimal :price, precision: 5, scale: 2
      t.integer :stock_leavel

      t.timestamps
    end
  end
end
