# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts "deleting previous data"
Product.destroy_all
Category.destroy_all
puts "Creating new default items"
(0..20).each do |i|
	category_name = "category#{i}"
	description = "category description.........................#{i}"
	Category.find_or_create_by(name: category_name,description: description)
end
(0..20).each do |i|
	product_name = "product#{i}"
	description = "product description.........................#{i}"
	quantity = i
	price = 100+i
	stock_leavel = 10+i
	Product.find_or_create_by(name: product_name,description: description,quantity: quantity, price: price,stock_leavel: stock_leavel)
end


