class UserMailer < ApplicationMailer
	default from: 'anees@railsfactory.org'
  
  def send_welcome_email(user)
    @user = user
    mail(:to => @user.email, :subject => "Welcome!")
  end
end
