// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require toastr
//= require jquery
//= require jquery_ujs
//= require jquery-1.10.2.min
//= require bootstrap.min
//= require menuzord
//= require jquery.flexslider-min
//= require owl.carousel.min
//= require jquery.isotope
//= require jquery.magnific-popup.min
//= require jquery.countTo
//= require breakpoint
//= require smooth
//= require wow.min
//= require imagesloaded
//= require turbolinks
//= require_tree .
